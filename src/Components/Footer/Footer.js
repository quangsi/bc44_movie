import React from "react";
import { Desktop, Mobile, Tablet } from "../../responsive/responsive";
import FooterDeskTop from "./FooterDeskTop";
import FooterMobile from "./FooterMobile";
import { Table } from "antd";
import FooterTablet from "./FooterTablet";

export default function Footer() {
  return (
    <div>
      <Desktop>
        <FooterDeskTop />
      </Desktop>
      <Tablet>
        <FooterTablet />
      </Tablet>
      <Mobile>
        <FooterMobile />
      </Mobile>
    </div>
  );
}
// interceptor axios

// 10 api => bật 10 lần , tắt 20 lần

// bật 1 , tắt 2 lần => tất cả page
