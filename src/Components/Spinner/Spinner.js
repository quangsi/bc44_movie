import React from "react";
import { useSelector } from "react-redux";
import { PacmanLoader } from "react-spinners";

export default function Spinner() {
  let { isLoading } = useSelector((state) => state.spinnerSlice);

  return isLoading ? (
    <div
      style={{ backgroundColor: "#023047" }}
      className="h-screen w-screen fixed top-0 left-0 z-20 flex justify-center items-center"
    >
      <PacmanLoader color="#ffb703" size={300} speedMultiplier={3} />
    </div>
  ) : (
    <></>
  );
}

// react spinner
