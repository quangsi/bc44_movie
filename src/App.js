import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import HomePage from "./Pages/HomePage/HomePage";
import LoginPage from "./Pages/LoginPage/LoginPage";
import DetailPage from "./Pages/DetailPage/DetailPage";
import NotFoundPage from "./Pages/404Page/404Page";
import Spinner from "./Components/Spinner/Spinner";
import Layout from "./Layout/Layout";

function App() {
  return (
    <div>
      <Spinner />
      <BrowserRouter>
        <Routes>
          {/* tạo 1 page: 1 path, 2 component */}

          <Route path="/" element={<Layout contentPage={<HomePage />} />} />
          <Route path="/login" element={<LoginPage />} />
          <Route
            path="/detail/:id"
            element={<Layout contentPage={<DetailPage />} />}
          />
          {/* <Route path="*" element={<NotFoundPage />} /> */}
          <Route path="/404" element={<NotFoundPage />} />
          <Route path="*" element={<Navigate to="/404" />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;

// login, register, home, detail, booking
